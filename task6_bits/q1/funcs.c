#include "funcs.h"


void checkMemoryAllocation(void *p) {
    if (p == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }
}

Client *createClient(int n) {
    Client *clients;
    int i;

    clients = (Client *) malloc(sizeof(Client) * n);
    checkMemoryAllocation(clients);

    for (i = 0; i < n; i++) {
        scanf("%s", clients[i].id);
        scanf("%s", clients[i].phone);
    }
    getchar();
    return clients;
}

ShortClient *createShortClientArr(int n) {
    ShortClient *compressedClients;
    Client *clients;
    int i;

    clients = createClient(n);

    compressedClients = (ShortClient *) malloc(sizeof(ShortClient) * n);
    checkMemoryAllocation(compressedClients);

    for (i = 0; i < n; i++) {
        compressIdNum(compressedClients[i].shortId, clients[i].id);
        compressPhoneNum(compressedClients[i].shortPhone, clients[i].phone);
    }

    free(clients);
    return compressedClients;
}

void compressIdNum(unsigned char *target, const char *src) {
    int i, j;

    for (i = j = 0; i < COMPRESS_ID_LEN; i++, j += 2) {
        target[i] = src[j] - '0';
        target[i] = target[i] << 4;
        target[i] = target[i] | (src[j + 1] - '0');
    }
}

void compressPhoneNum(unsigned char *target, const char *src) {
    int i, j;

    for (i = j = 0; i < COMPRESS_PHONE_LEN; i++, j++) {
        target[i] = src[j] - '0';
        target[i] = target[i] << 4;
        j == 2 ? j += 2 : j++;
        target[i] = target[i] | (src[j] - '0');
    }
}

char *searchClientByID(ShortClient *compressedClients, int n, char *id) {
    unsigned char lookFor[4];
    int i, j, foundIndex = -1, counter;
    char *found;

    compressIdNum(lookFor, id);

    for (i = 0; i < n; i++) {
        counter = 0;
        for (j = 0; j < COMPRESS_ID_LEN; j++) {
            if (compressedClients[i].shortId[j] == lookFor[j]) {
                counter++;
            }
            if (counter == 4) {
                foundIndex = i;
                break;
            }
        }
    }
    if (foundIndex == -1) {
        return NULL;
    }
    found = (char *) malloc(sizeof(char) * (PHONE_LEN + 1));
    checkMemoryAllocation(found);
    convertCompressPhone(found, compressedClients[foundIndex].shortPhone);
    return found;
}

void convertCompressPhone(char *target, const unsigned char *src) {
    unsigned char mask1, mask2;
    int i, j;

    mask1 = 0xF0;//(11110000)
    mask2 = 0xF;//(00001111)

    for (i = 0, j = 0; i < COMPRESS_PHONE_LEN; i++, j++) {
        target[j] = (char) (((src[i] & mask1) >> 4) + '0');
        j == 2 ? target[++j] = '-', j++ : j++;
        target[j] = (char) ((src[i] & mask2) + '0');
    }
    target[PHONE_LEN] = '\0';
}

