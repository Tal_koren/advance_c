#ifndef Q1_FUNCS_H
#define Q1_FUNCS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ID_LEN 8
#define COMPRESS_ID_LEN 4
#define PHONE_LEN 11
#define COMPRESS_PHONE_LEN 5

typedef struct client {
    char id[9];
    char phone[12];
} Client;

typedef struct shortClient {
    unsigned char shortId[4];
    unsigned char shortPhone[5];
} ShortClient;


void checkMemoryAllocation(void *p);

Client *createClient(int n);

void getData(char *field, int size);//Read data(phone num and id ) from user

void compressIdNum(unsigned char *target, const char *src);

void compressPhoneNum(unsigned char *target, const char *src);

ShortClient *createShortClientArr(int n);

void convertCompressPhone(char *target, const unsigned char *src);//Convert compress phone number to correct size

char *searchClientByID(ShortClient *compressedClients, int n, char *id);


#endif //Q1_FUNCS_H
