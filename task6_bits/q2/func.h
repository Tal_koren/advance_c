#ifndef Q2_FUNC_H
#define Q2_FUNC_H

#include <stdio.h>
#include <stdlib.h>

void checkMemoryAllocation(void *p);
int *filter(const int *numbers, int size, const unsigned char *pred, int *newSize);
int *xorFilter(int *numbers, int size, unsigned char *pred1, const unsigned char *pred2, int *newSize);

#endif //Q2_FUNC_H
