#include "func.h"

void checkMemoryAllocation(void *p) {
    if (p == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }
}

int *filter(const int *numbers, int size, const unsigned char *pred, int *newSize) {
    int *res, i, j, counter = 0;
    unsigned char mask;

    res = (int *) malloc(sizeof(int) * size);
    checkMemoryAllocation(res);

    for (i = 0; i < size / 8; i++) {
        mask = 1;
        for (j = 0; j < 8; j++) {
            if (pred[i] & mask) {
                res[counter++] = numbers[8 * i + j];
            }
            mask = mask << 1;
        }
    }
    res = (int *) realloc(res, sizeof(int) * counter);
    checkMemoryAllocation(res);
    *newSize = counter;

    return res;
}

int *xorFilter(int *numbers, int size,  unsigned char *pred1, const unsigned char *pred2, int *newSize) {
    int i;

    for (i = 0; i < size / 8; i++) {
        pred1[i] = pred1[i] ^ pred2[i];
    }
    return filter(numbers, size, pred1, newSize);
}