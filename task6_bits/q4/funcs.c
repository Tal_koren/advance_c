#include "funcs.h"

int *getIntArr(int *size) {
    int *arr, i;

    scanf("%d", size);

    arr = (int *) malloc(sizeof(int) * (*size));
    checkMemoryAllocation(arr);

    for (i = 0; i < (*size); i++) {
        scanf("%d", &arr[i]);
    }

    return arr;
}

int binSearch(void *arr, int size, int elemSize, void *item, int(*compare)(void *, void *)) {
    int left, right, mid, found = 0;

    left = 0;
    right = size - 1;

    while (!found && left <= right) {
        mid = (left + right) / 2;
        if (compare((BYTE *) arr + (mid * elemSize), (BYTE *)item) == 0) {
            found = 1;
            break;
        } else if (compare((BYTE *) arr + (mid * elemSize), (BYTE *)item) > 0) {
            right = mid - 1;
        } else {
            left = mid + 1;
        }
    }
    return found;
}

int compareInt(void *num1, void *num2) {
    return (*(int *) num1 - *(int *) num2);
}

void checkMemoryAllocation(void *p) {
    if (p == NULL) {
        printf("Memory Allocation failed\n");
        exit(1);
    }
}

int intBinSearch(int *arr, int size, int itemToFind) {
    return binSearch(arr, size, sizeof(int), &itemToFind, compareInt);
}

char **getStringArr(int *size) {
    char **arr;
    int i, len;
    char buffer[SIZE];

    scanf("%d", size);
    getchar();
    arr = (char **) malloc(sizeof(char *) * (*size));
    checkMemoryAllocation(arr);

    for (i = 0; i < (*size); i++) {
        gets(buffer);
        len = (int) strlen(buffer);
        arr[i] = (char *) malloc(sizeof(char) * (len + 1));
        checkMemoryAllocation(arr[i]);
        strcpy(arr[i], buffer);
        arr[i][len] = '\0';
    }

    return arr;
}

int compareStrings(void *str1, void *str2) {
    char **pString1 = str1;
    return strcmp(*pString1, (char *)str2);
}

int stringBinSearch(char **arr, int size, char *itemToFind) {
    return binSearch(arr, size, sizeof(char *), itemToFind, compareStrings);
}

void freeMemory(int *intArr, int intSize, char **strArr, int strSize) {
    int i;

    free(intArr);
    for (i = 0; i < strSize; i++) {
        free(strArr[i]);
    }
    free(strArr);
}