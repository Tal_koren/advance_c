#ifndef Q4_FUNCS_H
#define Q4_FUNCS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 100

typedef unsigned char BYTE;

int *getIntArr(int *size);

int compareInt(void *num1, void *num2);

void checkMemoryAllocation(void *p);

int intBinSearch(int *arr, int size, int itemToFind);

char **getStringArr(int *size);

int compareStrings(void *str1, void *str2);

int stringBinSearch(char **arr, int size, char *itemToFind);

void freeMemory(int *intArr, int intSize, char **strArr, int strSize);

#endif //Q4_FUNCS_H
