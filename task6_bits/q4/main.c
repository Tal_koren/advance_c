#include "funcs.h"

void main() {

    int *intArr;
    int intSize, intToFind;
    char **stringArr, stringToFind[SIZE];
    int stringSize;
    int res;


    intArr = getIntArr(&intSize);
    scanf("%d", &intToFind);
    res = intBinSearch(intArr, intSize, intToFind);
    if (res == 1){
        printf("The number %d was found\n", intToFind);
    }else{
        printf("The number %d was not found\n", intToFind);
    }

    stringArr = getStringArr(&stringSize);
    gets(stringToFind);

    res = stringBinSearch(stringArr, stringSize, stringToFind);
    if (res == 1){
        printf("The string %s was found\n", stringToFind);
    }else{
        printf("The string %s was not found\n", stringToFind);
    }
    freeMemory(intArr, intSize, stringArr, stringSize);
}






