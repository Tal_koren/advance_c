#include "funcs.h"

int *getIntArr(int *size) {
    int *arr, i;

    scanf("%d", size);

    arr = (int *) malloc(sizeof(int) * (*size));
    checkMemoryAllocation(arr);

    for (i = 0; i < (*size); i++) {
        scanf("%d", &arr[i]);
    }

    return arr;
}

int *scrambleInt(int *intArr, int size, int *indArr) {
    return (int *) scramble(intArr, sizeof(int), size, indArr);
}

void printIntArr(int *arr, int size) {
    printArr(arr, sizeof(int), size, printInt);
}

char **getStringArr(int *size) {
    char **arr;
    int i, len;
    char buffer[SIZE];

    scanf("%d", size);
    getchar();
    arr = (char **) malloc(sizeof(char *) * (*size));
    checkMemoryAllocation(arr);

    for (i = 0; i < (*size); i++) {
        gets(buffer);
        len = (int) strlen(buffer);
        arr[i] = (char *) malloc(sizeof(char) * (len + 1));
        checkMemoryAllocation(arr[i]);
        strcpy(arr[i], buffer);
        arr[i][len] = '\0';
    }

    return arr;
}

char **scrambleString(char **arr, int size, int *indArr) {
    return (char **) scramble(arr, sizeof(char *), size, indArr);
}

void printStringArr(char **strArr, int size) {
    printArr(strArr, sizeof(char *), size, printString);
}

void *scramble(void *arr, int elemSize, int n, int *indArr) {
    void *res;
    int i;

    res = malloc(elemSize * n);
    checkMemoryAllocation(res);

    for (i = 0; i < n; i++) {
        memcpy(((unsigned char *) res + (i * elemSize)), ((unsigned char *) arr + (indArr[i] * elemSize)), elemSize);
    }
    return res;
}

void checkMemoryAllocation(void *p) {
    if (p == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }
}

void freeMemory(int *intArr, int *scrambleIntArr, int intSize, char **stringArr, char **scrambleString, int strSize) {
    int i;

    free(intArr);
    free(scrambleIntArr);
    for (i = 0; i < strSize; i++) {
        free(stringArr[i]);
    }
    free(stringArr);
    free(scrambleString);
}

void printArr(void *arr, int elemSize, int size, void (*print)(void *)) {
    int i;

    for (i = 0; i < size; i++) {
        print((unsigned char *) arr + (i * elemSize));
    }
    printf("\n");

}

void printInt(void *num) {
    printf("%d ", *(int *) num);
}

void printString(void *str) {
    printf("%s\n", *(char **) str);
}

