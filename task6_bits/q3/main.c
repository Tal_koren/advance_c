#include "funcs.h"



void main() {
    int *intArr, *scrambleIntArr;
    int intSize;
    char **stringArr, **scrambleStringArr;
    int stringSize, i;
    int indArr[SIZE];

    intArr = getIntArr(&intSize);

    for (i = 0; i < intSize; i++) {
        scanf("%d", &indArr[i]);
    }

    scrambleIntArr = scrambleInt(intArr, intSize, indArr);
    printIntArr(scrambleIntArr, intSize);

    stringArr = getStringArr(&stringSize);

    for (i = 0; i < stringSize; i++) {
        scanf("%d", &indArr[i]);
    }

    scrambleStringArr = scrambleString(stringArr, stringSize, indArr);
    printStringArr(scrambleStringArr, stringSize);
    freeMemory(intArr, scrambleIntArr, intSize, stringArr, scrambleStringArr, stringSize);
}