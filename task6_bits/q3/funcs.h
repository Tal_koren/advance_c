#ifndef Q3_FUNCS_H
#define Q3_FUNCS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 100

void checkMemoryAllocation(void *p);

int *getIntArr(int *size);

char **getStringArr(int *size);

int* scrambleInt(int* intArr, int size, int* indArr);

void *scramble(void *arr, int elemSize, int n, int *indArr);

void printString(void *str);

void printInt(void *num);

void printArr(void *arr, int elemSize, int size, void (*print)(void *));

char **scrambleString(char **arr, int size, int *indArr);

void printStringArr(char **strArr, int size);

void printIntArr(int *arr, int size);

void freeMemory(int *intArr, int *scrambleIntArr, int intSize, char **stringArr, char **scrambleString, int strSize);
#endif //Q3_FUNCS_H
