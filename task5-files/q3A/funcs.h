#ifndef Q3A_FUNCS_H
#define Q3A_FUNCS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ADDTION ".ind"

typedef struct student {
    char *name;
    int avg;
} Student;

void Exe5Q3(char *fName);

void checkFileOpen(FILE *fp);

void checkMemoryAllocation(void *p);

Student *createStudentsArr(FILE *students, int **indexArr, short int *size);//Create student array and index array

void printStudents(Student *studentArr, short int size);//Test func

void printIndexArr(int *indexArr, short int size);//Test func

void freeStudentsArr(Student *studentsArr, short int size);

void
swapStudentsAndIndexes(Student *studentsArr, int *indexArr, int a, int b);//Swap arrays values according to student sort

void sortStudents(Student *studentArr, int *indexArr,
                  short int size);//sort student array and index array according to students
char *createIndexFileName(char *studentFileName, char *addition);

void writeIndexesToFile(FILE *indexFile, int *indexArr, short int size);

void printStudentsByIndexFile(char *indexFileName, char *studentFileName);//Test func

#endif //Q3A_FUNCS_H
