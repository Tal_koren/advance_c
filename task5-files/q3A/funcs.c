#include "funcs.h"

void Exe5Q3(char *fName) {
    FILE *studentFile, *indexFile;
    Student *students;
    int *indexArr;
    short int studentsSize;
    char *indexFileName;

    studentFile = fopen(fName, "rb");
    checkFileOpen(studentFile);

    students = createStudentsArr(studentFile, &indexArr, &studentsSize);
    sortStudents(students, indexArr, studentsSize);
    indexFileName = createIndexFileName(fName, ADDTION);
    indexFile = fopen(indexFileName, "w");
    checkFileOpen(indexFile);
    writeIndexesToFile(indexFile, indexArr, studentsSize);

    freeStudentsArr(students, studentsSize);
    free(indexArr);
    free(indexFileName);
    fclose(studentFile);
    fclose(indexFile);
}

void checkMemoryAllocation(void *p) {
    if (p == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }
}

void checkFileOpen(FILE *fp) {
    if (fp == NULL) {
        printf("Opening file failed\n");
        exit(1);
    }
}

Student *createStudentsArr(FILE *students, int **indexArr, short int *size) {
    Student *studentArr;
    short int tempNameSize;
    int *indexes;
    fread(size, sizeof(short int), 1, students);//read size of arr

    studentArr = (Student *) malloc(sizeof(Student) * (*size));
    checkMemoryAllocation(studentArr);
    indexes = (int *) malloc(sizeof(int) * (*size));
    checkMemoryAllocation(*indexArr);

    for (int i = 0; i < *size; i++) {
        indexes[i] = (int) ftell(students);//Read student index in the file to indexes arr
        fread(&tempNameSize, sizeof(short int), 1, students);//Read name size
        studentArr[i].name = (char *) malloc(sizeof(char) * (tempNameSize + 1));// allocate memory for name
        checkMemoryAllocation(studentArr[i].name);
        fread(studentArr[i].name, sizeof(char), tempNameSize, students);//read name
        studentArr[i].name[tempNameSize] = '\0';//add '\0' to close string
        fread(&studentArr[i].avg, sizeof(int), 1, students);//read avg
    }
    *indexArr = indexes;
    return studentArr;
}

void printStudents(Student *studentArr, short int size) {
    for (int i = 0; i < size; i++) {
        printf("Name: %s Avg: %d\n", studentArr[i].name, studentArr[i].avg);
    }
}

void printIndexArr(int *indexArr, short int size) {
    for (int i = 0; i < size; i++) {
        printf("student num : %d index : %d\n", i, indexArr[i]);
    }
}

void freeStudentsArr(Student *studentsArr, short int size) {
    for (int i = 0; i < size; i++) {
        free(studentsArr[i].name);
    }
    free(studentsArr);
}

void sortStudents(Student *studentArr, int *indexArr, short int size) {
    int min;

    for (int i = 0; i < size - 1; i++) {
        min = i;
        for (int j = i + 1; j < size; j++) {
            if (studentArr[min].avg > studentArr[j].avg) {
                min = j;
            }
        }
        swapStudentsAndIndexes(studentArr, indexArr, i, min);
    }
}

void swapStudentsAndIndexes(Student *studentsArr, int *indexArr, int a, int b) {
    Student tempStudent;
    int tempIndex;
    tempStudent = studentsArr[a];
    tempIndex = indexArr[a];
    studentsArr[a] = studentsArr[b];
    indexArr[a] = indexArr[b];
    studentsArr[b] = tempStudent;
    indexArr[b] = tempIndex;
}

char *createIndexFileName(char *studentFileName, char *addition) {
    int size1 = (int) strlen(studentFileName);
    int size2 = (int) strlen(addition);
    char *res;

    res = (char *) malloc(sizeof(char) * (size1 + size2 + 1));
    checkMemoryAllocation(res);

    strcpy(res, studentFileName);
    strcpy(res + size1, addition);
    res[size1 + size2] = '\0';

    return res;
}

void writeIndexesToFile(FILE *indexFile, int *indexArr, short int size) {
    fprintf(indexFile, "%d\n", size);//write file size
    for (int i = 0; i < size; i++) {
        fprintf(indexFile, "%d\n", indexArr[i]);
    }
}

void printStudentsByIndexFile(char *indexFileName, char *studentFileName) {
    FILE *indexFile, *studentsFile;
    short int size, tempNameSize;
    int curr, avg;
    char name[20];

    indexFile = fopen(indexFileName, "r");
    studentsFile = fopen(studentFileName, "rb");
    checkFileOpen(indexFile);
    checkFileOpen(studentsFile);

    fread(&size, sizeof(short int), 1, studentsFile);

    for (int i = 0; i < size; i++) {
        fscanf(indexFile, "%d\n", &curr);//read index
        fseek(studentsFile, curr, SEEK_SET);//Send cursor to index location
        //Read student details
        fread(&tempNameSize, sizeof(short int), 1, studentsFile);
        fread(name, sizeof(char), tempNameSize, studentsFile);
        name[tempNameSize] = '\0';
        fread(&avg, sizeof(int), 1, studentsFile);
        printf("name: %s avg: %d\n", name, avg);//print student details
    }
    fclose(indexFile);
    fclose(studentsFile);
}



