#ifndef TASK5_FILES_Q2FUNC_H
#define TASK5_FILES_Q2FUNC_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct employee {
    int nameLength;
    char *name;
    float salary;
}Employee;

void Ex5Q2(char* fName1, char *fName2);
Employee **buildArrayOfPointersToEmp(FILE *fp, int *size);
void updateSalaries(Employee **arrayOfPointersToEmp, FILE * fp, int size);//add to  each employee the match addition he needs to get
void swapEmployees(Employee **emp1, Employee **emp2);
void sortArrayOfPointersToEmp(Employee **arrayOfPointersToEmp, int size);
void updateFileOfEmployees(FILE *fp, Employee **arrayOfPointersToEmp, int size);
void freeArrayOfPointersToEmp(Employee **arrayOfPointersToEmp, int size);
void checkFileOpen(FILE *fp);
void checkMemoryAllocation(void *p);
#endif //TASK5_FILES_Q2FUNC_H
