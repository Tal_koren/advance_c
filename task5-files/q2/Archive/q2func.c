#include "q2func.h"

void Ex5Q2(char *fName1, char *fName2) {
    FILE *fp1, *fp2;
    Employee **arrayOfPointersToEmp;
    int size;

    fp1 = fopen(fName1, "rb+");
    fp2 = fopen(fName2, "rb");
    checkFileOpen(fp1);
    checkFileOpen(fp2);

    arrayOfPointersToEmp = buildArrayOfPointersToEmp(fp1, &size);
    updateSalaries(arrayOfPointersToEmp, fp2, size);
    sortArrayOfPointersToEmp(arrayOfPointersToEmp, size);
    updateFileOfEmployees(fp1, arrayOfPointersToEmp, size);

    freeArrayOfPointersToEmp(arrayOfPointersToEmp, size);
    fclose(fp1);
    fclose(fp2);
}

Employee **buildArrayOfPointersToEmp(FILE *fp, int *size) {
    Employee **res;
    int logSize = 0, phySize = 1, nameSize;

    res = (Employee **) malloc(sizeof(Employee *) * phySize);//Allocate memory to array of pointer
    checkMemoryAllocation(res);
    fseek(fp, 0, SEEK_SET);

    while (fread(&nameSize, sizeof(int), 1, fp) == 1) { //Read name size of employee
        res[logSize] = (Employee *) malloc(sizeof(Employee));// Allocate memory  employee
        checkMemoryAllocation(res[logSize]);
        res[logSize]->nameLength = nameSize;
        res[logSize]->name = (char *) malloc(sizeof(char) * (nameSize)); // Allocate memory to employee name
        checkMemoryAllocation(res[logSize]->name);
        fread(res[logSize]->name, sizeof(char), nameSize, fp); //Read name
        fread(&(res[logSize]->salary), sizeof(float), 1, fp);//Read salary
        logSize++;

        if (logSize == phySize) { //Check if there is enough space to scan more data
            phySize *= 2;
            res = (Employee **) realloc(res, phySize);
            checkMemoryAllocation(res);
        }
    }
    res = (Employee **) realloc(res, logSize + 1);
    *size = logSize;

    return res;
}

void updateSalaries(Employee **arrayOfPointersToEmp, FILE *fp, int size) {
    float tempAddition;
    for (int i = 0; i < size; i++) {
        fread(&tempAddition, sizeof(float), 1, fp);
        arrayOfPointersToEmp[i]->salary += tempAddition;
    }
}

void sortArrayOfPointersToEmp(Employee **arrayOfPointersToEmp, int size) {
    int max;

    for (int i = 0; i < size - 1; i++) {
        max = i;
        for (int j = i + 1; j < size; j++) {
            if (arrayOfPointersToEmp[max]->salary < arrayOfPointersToEmp[j]->salary) {
                max = j;
            }
        }
        swapEmployees(&(arrayOfPointersToEmp[i]), &(arrayOfPointersToEmp[max]));
    }
}

void swapEmployees(Employee **emp1, Employee **emp2) {
    Employee *temp;

    temp = *emp1;
    *emp1 = *emp2;
    *emp2 = temp;
}

void updateFileOfEmployees(FILE *fp, Employee **arrayOfPointersToEmp, int size) {
    fseek(fp, 0, SEEK_SET);

    for (int i = 0; i < size; i++) {
        fwrite(&(arrayOfPointersToEmp[i]->nameLength), sizeof(int), 1, fp); //Write name length
        fwrite(arrayOfPointersToEmp[i]->name, sizeof(char), arrayOfPointersToEmp[i]->nameLength, fp); //write name
        fwrite(&(arrayOfPointersToEmp[i]->salary), sizeof(float), 1, fp);// write salary
    }
}

void freeArrayOfPointersToEmp(Employee **arrayOfPointersToEmp, int size) {
    for (int i = 0; i < size; i++) {
        free(arrayOfPointersToEmp[i]->name);
        free(arrayOfPointersToEmp[i]);
    }
    free(arrayOfPointersToEmp);
}

void checkFileOpen(FILE *fp) {
    if (fp == NULL) {
        printf("Opening file failed\n");
        exit(1);
    }
}

void checkMemoryAllocation(void *p) {
    if (p == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }
}