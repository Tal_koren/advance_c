#include "funcs.h"

void main(int argc, char **argv) {
    char **records;
    int resSize = 0;

    records = findAverageGrade(argv[1], 99, &resSize);
    printNames(records, resSize);
    freeRecords(records, resSize);
}
