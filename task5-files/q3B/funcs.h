
#ifndef Q3B_FUNCS_H
#define Q3B_FUNCS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ADDITION ".ind"
#define NOT_FOUND -1

typedef struct student {
    char *name;
    int average;
} Student;

void printNames(char **records, int size);
char **findAverageGrade(char* database, int avgGrade, int * resSize);
void checkMemoryAllocation(void *p);
void checkFileOpen(FILE *fp);
Student *createStudentArr(FILE *indexFile, FILE *students, short int* studentArrSize);//creates student array by order of index file
void printStudents(Student *studentArr, short int size);//test func
void freeStudentsArr(Student *studentsArr, short int size);
int searchForAvg(Student *studentsArr, int size, int value);//Gets the index of first student the match value
char **buildStudentArrWithSameAvg(Student *studentsArr, int index, short int arrSize, int *resSize, int value); //build array of students names who's avg is match to value
void freeRecords(char **records, int size);
char *createIndexFileName(char *studentFileName, char *addition);//creates index file name base on students file
#endif //Q3B_FUNCS_H
