#include "funcs.h"

void printNames(char **records, int size) {
    int i;
    printf("The students names are:\n");
    for (i = 0; i < size; i++) {
        printf("%s\n", records[i]);
    }
}

char **findAverageGrade(char *database, int avgGrade, int *resSize) {
    FILE *students, *indexes;
    Student *studentsArr;
    char **res = NULL, *indexFileName;
    short int studentArrSize;
    int avgIndex;

    students = fopen(database, "rb");
    checkFileOpen(students);
    indexFileName = createIndexFileName(database, ADDITION);
    indexes = fopen(indexFileName, "r");
    checkFileOpen(indexes);

    studentsArr = createStudentArr(indexes, students, &studentArrSize);//creates for index file
    avgIndex = searchForAvg(studentsArr, studentArrSize, avgGrade);
    if (avgIndex != NOT_FOUND) {
        res = buildStudentArrWithSameAvg(studentsArr, avgIndex, studentArrSize, resSize, avgGrade);
    }
    freeStudentsArr(studentsArr, studentArrSize);
    fclose(students);
    fclose(indexes);
    return res;
}

void checkMemoryAllocation(void *p) {
    if (p == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }
}

void checkFileOpen(FILE *fp) {
    if (fp == NULL) {
        printf("Opening file failed\n");
        exit(1);
    }
}

Student *createStudentArr(FILE *indexFile, FILE *students, short int *studentArrSize) {
    short int size, tempNameSize;
    int tempIndex;
    Student *res;

    fread(&size, sizeof(short int), 1, students);//Read students array size
    res = (Student *) malloc(sizeof(Student) * size);
    checkMemoryAllocation(res);

    for (int i = 0; i < size; i++) {
        fscanf(indexFile, "%d\n", &tempIndex);//read index
        fseek(students, tempIndex, SEEK_SET);
        fread(&tempNameSize, sizeof(short int), 1, students);
        res[i].name = (char *) malloc(sizeof(char) * (tempNameSize + 1));
        checkMemoryAllocation(res[i].name);
        fread(res[i].name, sizeof(char), tempNameSize, students);
        res[i].name[tempNameSize] = '\0';
        fread(&(res[i].average), sizeof(int), 1, students);
    }
    *studentArrSize = size;
    return res;
}

void printStudents(Student *studentArr, short int size) {
    for (int i = 0; i < size; i++) {
        printf("Name: %s Avg: %d\n", studentArr[i].name, studentArr[i].average);
    }
}

void freeStudentsArr(Student *studentsArr, short int size) {
    for (int i = 0; i < size; i++) {
        free(studentsArr[i].name);
    }
    free(studentsArr);
}

int searchForAvg(Student *studentsArr, int size, int value) {
    int mid, right, left, res = -1;
    right = size - 1;
    left = 0;

    while (right >= left) {
        mid = left + (right - left) / 2;
        if (studentsArr[mid].average == value) {
            res = mid;
            break;
        } else if (studentsArr[mid].average < value) {
            left = mid + 1;

        } else {
            right = mid - 1;
        }
    }
    //Set index to the first student with the desired value
    while (studentsArr[res].average == value && res >= 0) {
        if(studentsArr[res - 1].average == value && res - 1 > 0){
            res --;
        } else{
            break;
        }
    }
    return res;
}

char **buildStudentArrWithSameAvg(Student *studentsArr, int index, short int arrSize, int *resSize, int value) {
    char **res;
    int logSize = 0, phySize = 1, tempNameSize;

    res = (char **) malloc(sizeof(char *) * phySize);
    checkMemoryAllocation(res);

    while (studentsArr[index].average == value && index < arrSize) {
        if (logSize == phySize) {
            phySize *= 2;
            res = (char **) realloc(res, sizeof(char *) * phySize);
            checkMemoryAllocation(res);
        }
        tempNameSize = (int) strlen(studentsArr[index].name);
        res[logSize] = (char *) malloc(sizeof(char) * (tempNameSize + 1));
        checkMemoryAllocation(res[logSize]);
        strcpy(res[logSize], studentsArr[index].name);
        res[logSize][tempNameSize] = '\0';
        logSize++;
        index++;
    }
    *resSize = logSize;
    res = (char **) realloc(res, sizeof(char *) * logSize);

    return res;
}

void freeRecords(char **records, int size){
    for(int i = 0; i < size; i++){
        free(records[i]);
    }
    free(records);
}

char *createIndexFileName(char *studentFileName, char *addition) {
    int size1 = (int) strlen(studentFileName);
    int size2 = (int) strlen(addition);
    char *res;

    res = (char *) malloc(sizeof(char) * (size1 + size2 + 1));
    checkMemoryAllocation(res);

    strcpy(res, studentFileName);
    strcpy(res + size1, addition);
    res[size1 + size2] = '\0';

    return res;
}