#ifndef TASK5_FILES_Q1FUNCS_H
#define TASK5_FILES_Q1FUNCS_H

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void Exe5Q1(char *fName, unsigned int x);

void checkFileOpen(FILE *fp); //Checks if file open correctly

void checkMemoryAllocation(void *p);

char **createSrtArray(FILE *input, unsigned int x);// Creates array of strings from binary file

char *cleanAndCopyStr(char *src);// Remove from each string capital letters

void swapStr(char **str1, char **str2);

void sortStrArray(char **strArray, unsigned int size);

void cleanStrArray(char **strArray, unsigned int size);// Remove capital letters and sort array

char *createOutputFName(char *inputName, char *addition);// creates new file name for out file based on input file

void freeStrArray(char **strArray, int size);

#endif //TASK5_FILES_Q1FUNCS_H
