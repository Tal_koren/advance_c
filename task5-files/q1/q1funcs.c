#include "q1funcs.h"

void Exe5Q1(char *fName, unsigned int x) {
    FILE *input, *output;
    char **strArray, *outFName;

    input = fopen(fName, "rb");
    checkFileOpen(input);

    strArray = createSrtArray(input, x); //Create array from file
    cleanStrArray(strArray, x);// Remove capitals and sort

    outFName = createOutputFName(fName, ".txt");
    output = fopen(outFName, "w");
    checkFileOpen(output);

    for (int i = 0; i < x; i++) { //Write str to txt file
        fprintf(output, "%s\n", strArray[i]);
    }
    freeStrArray(strArray, x);
    fclose(input);
    fclose(output);
}

char **createSrtArray(FILE *input, unsigned int x) {
    char **res;
    int tempSize;

    res = (char **) malloc(sizeof(char *) * x); // Allocate memory for array of strings
    checkMemoryAllocation(res);

    for (int i = 0; i < x; i++) {
        fread(&tempSize, sizeof(int), 1, input); //Read str size for the i-th place in array
        res[i] = (char *) malloc(
                sizeof(char) *
                (tempSize + 1)); // Allocated memory for the i-th string in array (+1 for '\0' at the end)
        checkMemoryAllocation(res[i]);
        fread(res[i], sizeof(char), tempSize, input);// Read string to array
        res[i][tempSize] = '\0';
    }

    return res;
}

void checkMemoryAllocation(void *p) {
    if (p == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }
}

void cleanStrArray(char **strArray, unsigned int size) {
    for (int i = 0; i < size; i++) {
        strArray[i] = cleanAndCopyStr(strArray[i]);
    }
    sortStrArray(strArray, size);
}

char *cleanAndCopyStr(char *src) {
    char *res;
    int size = (int) strlen(src), i, j;

    res = (char *) malloc(sizeof(char) * (size + 1));
    checkMemoryAllocation(res);

    for (i = j = 0; j < size; j++) {
        if (src[j] >= 'a' && src[j] <= 'z') {
            res[i] = src[j];
            i++;
        }
    }
    res[i] = '\0';
    res = (char *) realloc(res, i + 1);//Free unused memory if there is any;
    free(src);

    return res;
}

void sortStrArray(char **strArray, unsigned int size) {
    int minIndex;

    for (int i = 0; i < size - 1; i++) {
        minIndex = i;
        for (int j = i + 1; j < size; j++) {
            if (strcmp(strArray[minIndex], strArray[j]) > 0) {
                minIndex = j;
            }
        }
        if (strcmp(strArray[i], strArray[minIndex]) != 0) {
            swapStr(&strArray[i], &strArray[minIndex]);
        }
    }
}

void swapStr(char **str1, char **str2) {
    char *temp;

    temp = *str1;
    *str1 = *str2;
    *str2 = temp;
}

char *createOutputFName(char *inputName, char *addition) {
    char *res;
    int size1, size2;

    size1 = (int) strlen(inputName);
    size2 = (int) strlen(addition);

    res = (char *) malloc(sizeof(char) * (size1 + size2 + 1));
    checkMemoryAllocation(res);

    strcpy(res, inputName); //Copy str1 to res
    strcpy(res + size1, addition);// Continue copy str2 to res from where ended copy str1
    res[size1 + size2] = '\0';

    return res;

}

void freeStrArray(char **strArray, int size){
    for (int i = 0; i < size; i++){
        free(strArray[i]);
    }
    free(strArray);
}

